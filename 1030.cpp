#include <stdio.h>

int josephus(int n, int k)
{
  if (n == 1)
    return 1;
  else
    return (josephus(n - 1, k) + k-1) % n + 1;
}

int main() {

  int test_cases;
  scanf("%d", &test_cases);

  for (int i = 0; i < test_cases; i++) {
    int man, steps;
    scanf("%d %d", &man, &steps);
    printf("Case %d: %d\n", i+1, josephus(man, steps));
  }

  return 0;
}
