#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char * decodify (char * cod, int shift) {
  int i;
  char * decod = (char *)malloc(strlen(cod));
  for (i = 0; i < (int)strlen(cod); i++) {
    if ((cod[i] - shift) < 65){
      int tmp = cod[i] - 65;
      int new_shift = shift - tmp;
      decod[i] = 91 - new_shift;
    }
    else
      decod[i] = cod[i] - shift;
  }
  return decod;
}

int main() {
  int n_tests, n_shifts, i;
  char str[50];

  scanf("%d", &n_tests);
  for (i = 0; i < n_tests; i++) {
    scanf("%s", str);
    scanf("%d", &n_shifts);
    printf("%s\n", decodify(str, n_shifts));
  }

  return 0;
}
