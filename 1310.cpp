#include <stdio.h>

int kadane (int * array, int size) {

  int max_current, max_global;
  max_current = max_global = array[0];

  for (int i = 1; i < size; i++){
    if (array[i] > max_current + array[i]) max_current = array[i];
    else max_current = max_current + array[i];
    if (max_current > max_global) max_global = max_current;
  }

  return max_global;
}

int main() {

  int n_days, day_cost;

  while (scanf("%d", &n_days) != EOF){

    int day_revenue[n_days];

    scanf("%d", &day_cost);
    for (int i = 0; i < n_days; i++){
      scanf("%d", &day_revenue[i]);
      day_revenue[i] -= day_cost;
    }

    int result = kadane (day_revenue, n_days);
    if (result > 0)
      printf("%d\n", result);
    else
      printf("0\n");
  }

  return 0;
}
