#include <stdio.h>
#include <string.h>

int main() {

  int loops, i, q, j, k;
  int turn;
  char str1[50], str2[50], str3[100];

  scanf("%d", &loops);
  for (i = 0; i < loops; i++) {
    turn = 1;
    memset(str3, '\0', sizeof(str3));
    scanf("%s %s", str1, str2);

    q = k = j = 0;
    while (1) {
      if (str1[j] == '\0' && str2[k] == '\0')
        break;
      else if (str1[j] == '\0')
        str3[q] = str2[k++];
      else if (str2[k] == '\0')
        str3[q] = str1[j++];
      else if (turn == 1) {
        str3[q] = str1[j++];
        turn = 2;
      }
      else if (turn == 2) {
        str3[q] = str2[k++];
        turn = 1;
      }
      q++;
    }

    printf("%s\n", str3);
  }

  return 0;
}
