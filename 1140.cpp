#include <iostream>
#include <string.h>
#include <string>
#include <vector>

int main() {

  while (1) {
    std::string sentence;
    getline(std::cin, sentence);
    if (sentence == "*") break;

    char taut = 'N';

    std::vector<std::string> each_word;
    char * tokens = strtok((char *)sentence.c_str(), " ");

    while(tokens != NULL) {
      each_word.push_back(tokens);
      tokens = strtok(NULL, " ");
    }

    char first_letter = (*each_word.begin())[0];

    for (std::string str : each_word)
      if (str[0] == first_letter || str[0] == first_letter + 32 || str[0] == first_letter - 32) taut = 'Y';
      else {
        taut = 'N';
        break;
      }

    std::cout << taut << std::endl;
  }

  return 0;
}
