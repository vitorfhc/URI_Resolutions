#include <stdio.h>
#include <stdlib.h>

int ascending(void const *a, void const *b )
{
    return (*(int*)a - *(int*)b );
}

int main(){
    int n,i, actual, count;
    scanf("%d",&n);
    int num[n];

    for(i = 0; i < n; i++){
          scanf("%d",&num[i]);
    }

    qsort(num, n, sizeof(int), ascending);
    actual = num[0];
    i = 1;
    count = 1;
    while(1){
             if(i == n+1) break;
             if(num[i] == actual){
                       count++;
                       i++;
             }else{
                   printf("%d aparece %d vez(es)\n", actual, count);
                   actual = num[i];
                   count = 0;
             }
    }
    return 0;
}
