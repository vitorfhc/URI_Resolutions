#include <stdio.h>

int main() {

  int N, M;

  while (1) {
    scanf("%d %d", &N, &M); /* N NUMERO DE PARTICIPANTES E M NUMERO DE QUESTOES */
    if (N == 0 && M == 0) break;

    int achiv[4] = {1, 1, 1, 1};
    int matrix[N][M];

    for (int i = 0; i < N; i++) {
      for (int k = 0; k < M; k++)
        scanf("%d", &matrix[i][k]);
    }

    int questions[M];

    for (int i = 0; i < M; i++) questions[i] = 0;

    for (int i = 0; i < N; i++) {
      int team_solved = 0;

      for (int k = 0; k < M; k++) {
        team_solved += matrix[i][k];
        questions[k] += matrix[i][k];
      }

      if (team_solved == M) achiv[0] = 0;
      if (team_solved == 0) achiv[3] = 0;
    }

    for (int i = 0; i < M; i++) {
      if (questions[i] == 0) achiv[1] = 0;
      if (questions[i] == N) achiv[2] = 0;
    }

    int sum = 0;
    for (int i = 0; i < 4; i++) {
      sum += achiv[i];
    }

    printf("%d\n", sum);
  }

  return 0;
}
