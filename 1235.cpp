#include <stdio.h>
#include <string.h>

void read_string (char * str) {
  int i = 0;
  char c = getchar();
  while (c == '\n') c = getchar();
  do {
    str[i] = c;
    c = getchar();
    i++;
  } while (c != '\n');
}

int main () {
  int loops, i, k;
  char virus_str[100];
  int half, end;

  scanf("%d", &loops);
  for (i = 0; i < loops; i++) {

    memset(virus_str, '\0', sizeof(virus_str));
    read_string(virus_str);
    half = (strlen(virus_str) / 2) - 1;
    end = strlen(virus_str) - 1;

    k = half;
    while (1) {
      if (k == half + 1){
        printf("%c", virus_str[k]);
        break;
      }
      printf("%c", virus_str[k]);
      k--;
      if (k == -1) k = end;
    }
    printf("\n");
  }


  return 0;
}
