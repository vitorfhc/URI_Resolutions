#include <stdio.h>

int main() {
  int loop;
  scanf("%d", &loop);

  for (int i = 0; i < loop; i++) {
    char str[1000];
    int diamr = 0, diaml = 0;
    scanf("%s", str);

    for (unsigned int k = 0; str[k] != '\0'; k++) {
      if (str[k] == '<') diaml++;
      else if (str[k] == '>'){
        if (diaml > 0){
          diamr++;
          diaml--;
        }
      }
    }

    printf("%d\n", diamr);

  }

  return 0;
}
