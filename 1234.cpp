#include <stdio.h>
#include <iostream>

int main() {
  int k, upper;
  char string[50];

  while (std::cin.getline(string, 50)){
      upper = 1;
      k = 0;

      while (string[k] != '\0'){
        if (string[k] >= 97 && string[k] <= 122 && upper == 1){
          string[k] -= 32;
          upper = 0;
        }
        else if (string[k] >= 65 && string[k] <= 90 && upper == 1){
          upper = 0;
        }
        else if (string[k] >= 97 && string[k] <= 122 && upper == 0){
          upper = 1;
        }
        else if (string[k] >= 65 && string[k] <= 90 && upper == 0){
          string[k] += 32;
          upper = 1;
        }
        k++;
      }

    printf("%s\n", string);
  }
  return 0;
}
