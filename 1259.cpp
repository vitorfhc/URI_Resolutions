#include <stdio.h>
#include <stdlib.h>

void quicksort(int * array, int begin, int end);
int partition (int * array, int begin, int end);
void swap (int * a, int * b);

void quicksort(int * array, int begin, int end) {
  if (begin >= end) return;
  int pIndex = partition(array, begin, end);
  quicksort(array, begin, pIndex - 1);
  quicksort(array, pIndex + 1, end);
  return;
}

int partition (int * array, int begin, int end) {
  int pIndex = begin;
  int pivot = array[end];
  for (int i = begin; i < end; i++){
    if (array[i] <= pivot){
      swap(array + i, array + pIndex);
      pIndex++;
    }
  }
  swap(array + pIndex, array + end);
  return pIndex;
}

void swap (int * a, int * b) {
  int tmp = *a;
  *a = *b;
  *b = tmp;
  return;
}

int main() {
  int n_numbers, tmp;
  int * even, * odd;
  scanf("%d", &n_numbers);

  even = (int *)malloc(sizeof(int) * n_numbers);
  odd = (int *)malloc(sizeof(int) * n_numbers);

  int e = 0, o = 0;
  for (int i = 0; i < n_numbers; i++) {
    scanf("%d", &tmp);
    if (tmp % 2)
      odd[o++] = tmp;
    else
      even[e++] = tmp;
  }

  quicksort(odd, 0, o - 1);
  quicksort(even, 0, e - 1);

  int k = 0;
  while (k < e)
    printf("%d\n", even[k++]);

  k = o - 1;
  while (k >= 0)
    printf("%d\n", odd[k--]);

  return 0;
}
