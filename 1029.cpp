#include <stdio.h>

int calls;

int fib(int n) {
  calls++;
  if (n == 0) return 0;
  else if (n == 1) return 1;
  else return fib(n-1) + fib (n-2);
}

int main () {

   int test_cases;
   scanf("%d", &test_cases);

   for (int i = 0; i < test_cases; i++) {
     int number;
     calls = 0;
     scanf("%d", &number);

     printf("fib(%d) = %d calls = %d\n", number, calls - 1, fib(number));
   }

  return 0;
}
