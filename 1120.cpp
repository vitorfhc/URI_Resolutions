#include <stdio.h>
#include <string.h>

int main() {

  int idigit, i, k;
  char number[101];
  char digit;
  char new_number[101];

  while(1) {
    memset(number, '\0', sizeof(number));
    memset(new_number, '\0', sizeof(new_number));
    scanf("%d %s", &idigit, number);
    if (idigit == 0 && number[0] == '0') break;
    digit = idigit + '0';

    i = 0;
    k = 0;
    while (number[i] != '\0'){
      if (number[i] != digit){
        new_number[k] = number[i];
        k++;
      }
      i++;
    }

    k = 0;
    if (new_number[k] == '\0') printf("0");
    while (new_number[k] == '0'){
      k++;
      if (new_number[k] == '\0'){
        printf("0");
        break;
      }
    }
    while (new_number[k] != '\0'){
      if (new_number[k] != '\0') printf("%c", new_number[k]);
      k++;
    }
    printf("\n");
  }

  return 0;
}
