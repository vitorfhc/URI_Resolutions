#include <string>
#include <iostream>
#include <sstream>

int main() {

  int loops;
  std::cin >> loops;
  std::cin.ignore(1);

  for (int i = 0; i < loops; i++) {
    std::string linha;
    getline(std::cin, linha);

    std::stringstream palavras (linha);

    std::string palavra;
    while (palavras >> palavra)
      std::cout << palavra[0];
    std::cout << std::endl;
  }

  return 0;
}
