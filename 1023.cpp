#include <stdio.h>
#include <stdlib.h>
#include <map>
#include <math.h>

int main() {

  int n_casas, i, ncidade = 0;

  while (1) {
    scanf("%d", &n_casas);
    if (n_casas == 0) break;

    std::map<int, int> casa; /* media de agua por pessoa / quantidade de pessoas */
    int total_agua, total_pessoas, pessoas, agua;
    total_agua = total_pessoas = 0;

    for (i = 0; i < n_casas; i++){
      scanf("%d %d", &pessoas, &agua);
      total_pessoas += pessoas;
      total_agua += agua;

      std::pair<std::map<int,int>::iterator, bool> pv;
      pv = casa.insert(std::pair<int,int>(agua/pessoas, pessoas));
      if (pv.second == false) casa[agua/pessoas] += pessoas;
    }

    ncidade++;
    printf("Cidade# %d:\n", ncidade);

    std::map<int, int>::iterator fim = casa.end();
    fim--;

    for(std::map<int, int>::iterator it = casa.begin(); it != casa.end(); it++){
      if (it == fim) printf("%d-%d\n", it->second, it->first);
      else printf("%d-%d ", it->second, it->first);
    }

    printf("Consumo medio: %.2lf m3.\n", floor(100 * (float)total_agua/total_pessoas) / 100);

  }

  return 0;
}
